from django.urls import path
from todos.views import show_list

urlpatterns = [
    path("todos/", show_list, name="show_list"),
]
